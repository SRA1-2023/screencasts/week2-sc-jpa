import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
	private static Logger log = LoggerFactory.getLogger( App.class );
	
	public static void main(String argv[]) {
		System.out.println("Running App ...");
		
		log.debug("Create persistence manager");
		
		log.debug("Create entities");
		
		log.debug("Close Entity Manager");
		

	}
}